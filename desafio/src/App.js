import React, { Component } from 'react'
import Input from './Input'
import NumberPrice from './NumberPrice'
import FormattedPrice from './FormattedPrice'
import WelcomeDialog from './WelcomeDialog'
import './App.css'

class App extends Component {
	state = {
		firstName: '',
		price: 0,
		priceList: []
    }

    changePrice = value => {
		const intValue = parseInt(value)
		if (typeof intValue === "number" && intValue !== 0) {
			this.setState({ price: intValue })
		}
	}
	
    savePrice = (event) => {
		event.preventDefault()
		const { state: { priceList } } = this
		priceList.push(this.state.price)

		this.setState({ priceList }) // { priceList: priceList }
	}
	
	onRemove = (index) => {
		const { state: { priceList } } = this
		priceList.splice(index, 1)
		
		this.setState({ priceList }) // { priceList: priceList }
	}
	
	render() {
		return (
			<>
				<form className="app" onSubmit={(event) => this.savePrice(event)}>
					<Input changeValue={this.changePrice} value={this.state.price} />
					<Input changeValue={(value) => this.setState({ firstName: value })} value={this.state.first} />
					<button>salvar</button>
				</form>
				<div className="list">
					{this.state.priceList.map((price, index) => {
						return <FormattedPrice 
						key={`${price}-${index}`} 
						price={price} 
						index={index} 
						onRemove={this.onRemove}
					/>
					})}
				</div>
			</>
		)
	}
}

export default App